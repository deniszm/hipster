package com.mycompany.myapp.service

class EmailAlreadyUsedException : RuntimeException("Email is already in use!")
