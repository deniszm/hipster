package com.mycompany.myapp.service

class UsernameAlreadyUsedException : RuntimeException("Login name already used!")
