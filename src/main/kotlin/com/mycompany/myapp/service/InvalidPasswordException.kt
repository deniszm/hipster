package com.mycompany.myapp.service

class InvalidPasswordException : RuntimeException("Incorrect password")
